# Screenshoots:

Register page |          Laying Out User Interface
:-------------------------:|:-------------------------:
![Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.33.31](/uploads/89ff35e06587c8b3652ae5f618467deb/Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.33.31.png) | ![Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.34.45](/uploads/2606b94c84493f229ef90be51b9c11e1/Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.34.45.png)

User Details  |          It's a Match
:-------------------------:|:-------------------------:
![772ef0b4-46a5-416c-8f85-3b171204b61b](/uploads/ff1e6828cba74d7d8a2b6548e9eeeda0/772ef0b4-46a5-416c-8f85-3b171204b61b.png) | ![Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.41.12](/uploads/3eaea357354ed57b031f7ba89ed8d6bf/Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.41.12.png)

Recent Messages  |          Chat
:-------------------------:|:-------------------------:
![Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.46.38](/uploads/72d16176428203ab7f58dab15f2e4101/Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.46.38.png) | ![Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.47.30](/uploads/d07db358c8ac08eea1ad7576b6da944d/Simulator_Screen_Shot_-_iPhone_11_-_2021-02-04_at_14.47.30.png)
