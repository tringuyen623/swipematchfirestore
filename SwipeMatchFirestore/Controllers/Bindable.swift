//
//  Bindable.swift
//  SwipeMatchFirestore
//
//  Created by Tri Nguyen on 5/13/20.
//  Copyright © 2020 Tri Nguyen. All rights reserved.
//

import Foundation

class Bindable<T> {
    var value: T? {
        didSet {
            observer?(value)
        }
    }
    
    var observer: ((T?) -> ())?
    
    func bind(observer: @escaping (T?) -> ()) {
        self.observer = observer
    }
}
