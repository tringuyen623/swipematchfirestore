//
//  ChatLogController.swift
//  SwipeMatchFirestore
//
//  Created by Tri Nguyen on 7/22/20.
//  Copyright © 2020 Tri Nguyen. All rights reserved.
//

import LBTATools
import Firebase

class ChatLogController: LBTAListController<MessageCell, Message>, UICollectionViewDelegateFlowLayout {
    
    fileprivate lazy var customNavBar = MessagesNavBar(match: match)
    
    fileprivate let navBarHeight: CGFloat = 120
    
    fileprivate let match: Match
    
    init(match: Match) {
        self.match = match
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("CHATLOGCONTROLLER ---- Object is destroying itself properly, no retain cycles or any other memory related issues. Memory being reclaimed properly")
    }
    
    lazy var customInputView: CustomInputAccessView = {
        let civ = CustomInputAccessView(frame: .init(x: 0, y: 0, width: view.frame.width, height: 50))
        civ.sendButton.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        
        return civ
    }()
    
    override var inputAccessoryView: UIView? {
        get {
            return customInputView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    var listener: ListenerRegistration?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.keyboardDismissMode = .interactive
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleShowKeyboard), name: UIResponder.keyboardDidShowNotification, object: nil)
        
        fetchMessages()
        
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isMovingFromParent {
            listener?.remove()
        }
    }
    
    @objc fileprivate func handleShowKeyboard() {
        self.collectionView.scrollToItem(at: [0, items.count - 1], at: .bottom, animated: true)
    }
    
    fileprivate func fetchMessages() {
        print("Fetching messages...")
        
        guard let currentUserId = Auth.auth().currentUser?.uid else { return }
        
        let query = Firestore.firestore().collection("matches_messages").document(currentUserId).collection(match.uid).order(by: "timestamp")
        
        listener = query.addSnapshotListener { (querySnapshot, error) in
            if let error = error {
                print("Failed to fetch messages:", error)
                return
            }
            
            querySnapshot?.documentChanges.forEach({ (change) in
                if change.type == .added {
                    let dictionary = change.document.data()
                    self.items.append(.init(dictionary: dictionary))
                }
            })
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: [0, self.items.count - 1], at: .bottom, animated: true)
        }
    }
    
    fileprivate func setupUI() {
        view.addSubview(customNavBar)
        customNavBar.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, size: .init(width: 0, height: navBarHeight))
        
        customNavBar.backButton.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        
        collectionView.contentInset.top = navBarHeight
        collectionView.verticalScrollIndicatorInsets.top = navBarHeight
        
        let statusBarCover = UIView(backgroundColor: .white)
        view.addSubview(statusBarCover)
        statusBarCover.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.topAnchor, trailing: view.trailingAnchor)
    }
    
    @objc fileprivate func handleBack() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc fileprivate func handleSend() {
        
        saveToFromMessage()
        saveRecentToFromMessage()
    }
    
    fileprivate func saveRecentToFromMessage() {
        guard let currentUserId = Auth.auth().currentUser?.uid else { return }
        
        let collection = Firestore.firestore().collection("matches_messages").document(currentUserId).collection("recent_messages").document(match.uid)
        
        let data: [String:Any] = [
            "text": customInputView.textView.text ?? "",
            "name": match.name,
            "uid": match.uid,
            "profileImageUrl": match.profileImageUrl,
            "timestamp": Timestamp(date: Date())
        ]
        
        collection.setData(data) { (error) in
            if let error = error {
                print("Failed to save message:", error)
                return
            }
            
            print("Successfully saved recent msg into Firestore")
        }
        
        let currentUserName = UserDefaulsManager.getUserName()
        let currentUserProfileImageURL = UserDefaulsManager.getUserProfileImageString()
        
        let toData: [String:Any] = [
            "text": customInputView.textView.text ?? "",
            "name": currentUserName ?? "",
            "uid": currentUserId,
            "profileImageUrl": currentUserProfileImageURL ?? "",
            "timestamp": Timestamp(date: Date())
        ]
        
        Firestore.firestore().collection("matches_messages").document(match.uid).collection("recent_messages").document(currentUserId).setData(toData)
    }
    
    fileprivate func saveToFromMessage() {
        guard let currentUserId = Auth.auth().currentUser?.uid else { return }
        
        let collection = Firestore.firestore().collection("matches_messages").document(currentUserId).collection(match.uid)
        let toCollection = Firestore.firestore().collection("matches_messages").document(match.uid).collection(currentUserId)
        
        let data: [String:Any] = [
            "text": customInputView.textView.text ?? "",
            "fromId": currentUserId,
            "toId": match.uid,
            "timestamp": Timestamp(date: Date())
        ]
        
        collection.addDocument(data: data) { (error) in
            if let error = error {
                print("Failed to save message:", error)
                return
            }
            
            self.customInputView.textView.text = nil
            self.customInputView.placeholderLabel.isHidden = false
            
            print("Successfully saved msg into Firestore")
        }
        
        toCollection.addDocument(data: data) { (error) in
            if let error = error {
                print("Failed to save message:", error)
                return
            }
            
            self.customInputView.textView.text = nil
            self.customInputView.placeholderLabel.isHidden = false
            
            print("Successfully saved msg into Firestore")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 16, left: 0, bottom: 16, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let estimatedSizeCell = MessageCell(frame: .init(x: 0, y: 0, width: view.frame.width, height: 10))
        estimatedSizeCell.item = items[indexPath.item]
        
        estimatedSizeCell.layoutIfNeeded()
        
        let estimatedSize = estimatedSizeCell.systemLayoutSizeFitting(.init(width: view.frame.width, height: 10))
        
        return .init(width: view.frame.width, height: estimatedSize.height)
    }
}
