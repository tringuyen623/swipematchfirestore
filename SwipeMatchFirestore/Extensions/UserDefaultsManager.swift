//
//  UserDefaultsManager.swift
//  SwipeMatchFirestore
//
//  Created by Tri Nguyen on 8/4/20.
//  Copyright © 2020 Tri Nguyen. All rights reserved.
//

import Foundation

class UserDefaulsManager {
    
    static func setUserName(userName: String) {
        UserDefaults.standard.set(userName, forKey: "userName")
    }
    
    static func getUserName() -> String? {
        let userName = UserDefaults.standard.string(forKey: "userName")
        return userName
    }
    
    static func setUserUid(uid: String) {
        UserDefaults.standard.set(uid, forKey: "userUid")
    }
    
    static func getUserUid() -> String? {
        let userUid = UserDefaults.standard.string(forKey: "userUid")
        return userUid
    }
    
    static func setUserProfileImageString(profileImageString: String) {
        UserDefaults.standard.set(profileImageString, forKey: "userProfileImage")
    }
    
    static func getUserProfileImageString() -> String? {
        let userProfileImage = UserDefaults.standard.string(forKey: "userProfileImage")
        return userProfileImage
    }
    
    static func removeUser() {
        if let appDomain = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
    }
}
