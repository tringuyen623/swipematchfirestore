//
//  Match.swift
//  SwipeMatchFirestore
//
//  Created by Tri Nguyen on 8/1/20.
//  Copyright © 2020 Tri Nguyen. All rights reserved.
//

import Foundation

struct Match {
    var name, profileImageUrl, uid: String
    
    init(dictionary: [String:Any]) {
        self.name = dictionary["name"] as? String ?? ""
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
    }
}
