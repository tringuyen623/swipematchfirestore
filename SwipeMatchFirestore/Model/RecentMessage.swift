//
//  RecentMessage.swift
//  SwipeMatchFirestore
//
//  Created by Tri Nguyen on 8/4/20.
//  Copyright © 2020 Tri Nguyen. All rights reserved.
//

import Foundation
import Firebase

struct RecentMessage {
    let text, uid, name, profileImageUrl: String
    let timeStamp: Timestamp
    
    init(dictionary: [String: Any]) {
        self.text = dictionary["text"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
        self.name = dictionary["name"] as? String ?? ""
        self.profileImageUrl = dictionary["profileImageUrl"] as? String ?? ""
        
        self.timeStamp = dictionary["timestamp"] as? Timestamp ?? Timestamp(date: Date())
    }
    
    func toDictionary() -> [String: Any] {
        var dictionary = [String:Any]()
        
        dictionary["uid"] = uid
        dictionary["name"] = name
        dictionary["profileImageUrl"] = profileImageUrl
        dictionary["text"] = text
        
        return dictionary
    }
}
