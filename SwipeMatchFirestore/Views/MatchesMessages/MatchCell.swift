//
//  MatchCell.swift
//  SwipeMatchFirestore
//
//  Created by Tri Nguyen on 8/1/20.
//  Copyright © 2020 Tri Nguyen. All rights reserved.
//

import LBTATools

class MatchCell: LBTAListCell<Match> {
    let profileImage = UIImageView(image: #imageLiteral(resourceName: "kelly1"), contentMode: .scaleAspectFill)
    let userNameLabel = UILabel(text: "Username Here", font: .systemFont(ofSize: 14, weight: .semibold), textColor: #colorLiteral(red: 0.2156666517, green: 0.2156972885, blue: 0.2156562209, alpha: 1), textAlignment: .center, numberOfLines: 2)
    
    override var item: Match! {
        didSet {
            userNameLabel.text = item.name
            profileImage.sd_setImage(with: URL(string: item.profileImageUrl))
        }
    }
    
    override func setupViews() {
        super.setupViews()
        
        profileImage.clipsToBounds = true
        profileImage.constrainHeight(80)
        profileImage.constrainWidth(80)
        profileImage.layer.cornerRadius = 80 / 2
        
        stack(stack(profileImage, alignment: .center),userNameLabel)
    }
}
