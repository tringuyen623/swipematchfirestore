//
//  MatchesHeader.swift
//  SwipeMatchFirestore
//
//  Created by Tri Nguyen on 8/1/20.
//  Copyright © 2020 Tri Nguyen. All rights reserved.
//

import LBTATools

class MatchesHeader: UICollectionReusableView {
    let newMatchesLabel = UILabel(text: "New matches", font: .boldSystemFont(ofSize: 18), textColor: #colorLiteral(red: 1, green: 0.3098039216, blue: 0.368627451, alpha: 1))
    let matchesHorizontalController = MatchesHorizontalController()
    let messagesLabel = UILabel(text: "Messages", font: .boldSystemFont(ofSize: 18), textColor: #colorLiteral(red: 1, green: 0.3098039216, blue: 0.368627451, alpha: 1))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        stack(stack(newMatchesLabel).padLeft(20),
              matchesHorizontalController.view,
              stack(messagesLabel).padLeft(20),
              spacing: 20
        ).withMargins(.init(top: 20, left: 0, bottom: 8, right: 0))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
