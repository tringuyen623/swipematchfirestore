//
//  RecentMessageCell.swift
//  SwipeMatchFirestore
//
//  Created by Tri Nguyen on 8/4/20.
//  Copyright © 2020 Tri Nguyen. All rights reserved.
//

import LBTATools

class RecentMessageCell: LBTAListCell<RecentMessage> {
    
    let userProfileImage = UIImageView(image: #imageLiteral(resourceName: "lady4c.jpg"), contentMode: .scaleAspectFill)
    let usernameLabel = UILabel(text: "User Name", font: .boldSystemFont(ofSize: 18))
    let messageTextLabel = UILabel(text: "Some long line of text should spand 2 lines Some long line of text should spand 2 lines", font: .systemFont(ofSize: 16), textColor: .gray, numberOfLines: 2)
    
    override var item: RecentMessage! {
        didSet {
            usernameLabel.text = item.name
            messageTextLabel.text = item.text
            userProfileImage.sd_setImage(with: URL(string: item.profileImageUrl))
        }
    }
    
    override func setupViews() {
        super.setupViews()
        
        userProfileImage.layer.cornerRadius = 94 / 2
        
        hstack(userProfileImage.withWidth(94).withHeight(94),
               stack(usernameLabel, messageTextLabel, spacing: 2),
               spacing: 20,
               alignment: .center
            ).padLeft(20).padRight(20)
        
        addSeparatorView(leadingAnchor: usernameLabel.leadingAnchor)
    }
}
